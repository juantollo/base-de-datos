SELECT t.Name as track, g.Name as genre, m.Name as media 
FROM Track t
    INNER JOIN Genre g ON t.GenreId = g.GenreId
    INNER JOIN MediaType m ON t.MediaTypeId = m.MediaTypeId


SELECT count(*) as Frecuencia, g.Name 
FROM Track t
    INNER JOIN Genre g ON t.GenreId = g.GenreId
    GROUP BY g.Name

SELECT a.Name
FROM Artist a 
EXCEPT
SELECT a.Name
FROM Artist a 
    INNER JOIN Album al ON a.ArtistId = al.ArtistId

