SELECT P.Name , P.ProductNumber
FROM Production.Product P
WHERE ProductNumber ='EC-R098'

SELECT P.ProductID , P.ProductNumber
FROM Production.Product P
WHERE ProductNumber ='EC-R098'

--En la 2da busca directamente en el indice uncluster de numero de producto
-- que ya esta ordenado y devuelve ese registro. Luego hace la seleccion.
-- En cambio en la 1ra tiene que entrar a la tabla y revisar valores especificos de una hoja.

--0.2. Estas dos consultas son similares sin embargo los planes son diferentes. Explique lo que est�a ocurriendo
SELECT SalesOrderID, SalesOrderDetailID
FROM Sales.SalesOrderDetail
WHERE SalesOrderID = 58950

SELECT SalesOrderID, SalesOrderDetailID
FROM Sales.SalesOrderDetail
WHERE SalesOrderDetailID = 68531

-- En la 1era buscar por pk en un indice clustered SalesOrderDetail es rapido.
-- En la segunda consulta hace algo extrano. Revisa un indice que puede
-- no tener valores unicos y hace un scan. Entonces busca entre los products ids de las ventas los que tengan asociado esa primary key_

--0.3. Mire detalladamente estas consultas y explique los planes de ejecuci�on
SELECT SalesOrderID, SalesOrderDetailID
FROM Sales.SalesOrderDetail
WHERE SalesOrderID = 43683 AND SalesOrderDetailID = 240

SELECT SalesOrderID, SalesOrderDetailID
FROM Sales.SalesOrderDetail
WHERE SalesOrderID = 43683 OR SalesOrderDetailID = 240

-- En el primer caso si bien se busca en un indice cluster es rapido porque se usala 
-- la PK y se pide una condicion mas a cumplir. En cambio en la 2da se revisa uno por 
-- uno en un scan que se cumpla una u otra condicion.

--0.4. Compare los planes de ejecuci�on de las siguientes tres consultas. Explique que hace cada
--uno y por qu�e se eligen mecanismos de junta diferentes.

SELECT ProductID, PV.BusinessEntityID, Name
FROM Purchasing.ProductVendor PPV JOIN Purchasing.Vendor PV
ON (PPV.BusinessEntityID = PV.BusinessEntityID)

-- Quiiero los productID y PV.BusinessEntityID de los BusinessEntityID que esten en  Purchasing.Vendor y Purchasing.ProductVendor
-- En PPV tenemos un indice unclustered con los BusinessEntityID que como es non-unique puede tener repeticiones y obliga a un scan,
-- pero todavia es un scan de un indice unclustered. 
-- BusinessEntityID y ProductID son PK en PV por lo que es facil buscar ahi, tal vez hace un scan porque son pocos registros.
-- Interpreto entonces hacer un join usando estos dos indices es lo m�s eficiente porque por un lado el primer indice
-- tiene pocos registros por lo que se puede hacer un scan y devolver los valores que luego se seleccionan, y matchear
-- rapido con los BusinessEntityID que tambien estan en la otra tabla.

SELECT ProductID, PV.BusinessEntityID, Name
FROM Purchasing.ProductVendor PPV JOIN Purchasing.Vendor PV
ON (PPV.BusinessEntityID = PV.BusinessEntityID)
WHERE StandardPrice > $10

-- El costo de l aconsulta aumenta en este caso porque para verificar la condcion del precio se mete ahora en dos
-- indices clustereds.

SELECT ProductID, PV.BusinessEntityID, Name
FROM Purchasing.ProductVendor PPV JOIN Purchasing.Vendor PV
ON (PPV.BusinessEntityID =PV.BusinessEntityID)
WHERE StandardPrice > $10 AND Name LIKE N'F%'
-- Lo primero que hace es buscar en el indice clustered de PV los negocios que cumplen la condicion del nombre. Es rapido porque la tabla tiene pocos registros.
-- Luego revisa el indice unclustered los BusinessEntityID y merge con la tabla anterior haciendo un neested loop.
-- Luego al igual que las consultas anteriores como se devuelve el ProductID se va al indice clustered y se revisan los registros con BusinessEntityID 
-- que cumplen las condiciones anteriores. En este caso se van a matchear pocos registros por lo que el motor elige
-- un keylook up en lugar de un scan. 

--0.5. Compare los planes de ejecuci�on de las siguientes consultas. Explique que hace cada uno
--y en que se basa la elecci�on del optimizador

SELECT P.Name, PSC.Name SubCatrom
FROM Production.Product P
JOIN Production.ProductSubcategory PSC
ON p.ProductSubcategoryID = psc.ProductSubcategoryID

-- Tenemos un join entre P y PSC que devuelve columnas de ambas tablas.
-- Para joinear por ProductSubcategoryID en la tabla PSC, como solo me interesa PSC.Name,
-- se usa el indice AK_ProductSubcategory_Name que tiene la PK con la que joineamos y la AK que nos pide la consulta.
-- por otro lado para traer P.Name realiza un scan para chequear los registros no nullos de ProductSubcategoryID.
-- Finalmente hace un join con un hash match para optimizar el join. 

SELECT P.Name, PSC.Name SubCatrom
FROM Production.Product P
JOIN Production.ProductSubcategory PSC
ON p.ProductSubcategoryID = psc.ProductSubcategoryID
ORDER BY psc.ProductSubcategoryID

-- PSC es una tabla peque�a entonces ni cuesta mucho buscar el PSC.Name. Luego, en la tabla 
-- P busca los registros no nullos de p.ProductSubcategoryID y los  ordena por p.ProductSubcategoryID.


--0.6. Compare las siguientes dos consultas y explique la diferencia de planes
SELECT count(NameStyle) FROM Person.Person
SELECT count(Title) FROM Person.Person

-- La segunda es mucho mas costosa por Title puede ser nulo y naleStyle no.

--0.7. Analice los planes de las siguientes consultas
SELECT jc.Resume FROM HumanResources.JobCandidate jc
INNER JOIN HumanResources.Employee e on jc.BusinessEntityID =e.BusinessEntityID
ORDER BY e.BusinessEntityID,jc.JobCandidateID

SELECT JobCandidateID FROM HumanResources.JobCandidate jc
INNER JOIN HumanResources.Employee e on jc.BusinessEntityID =e.BusinessEntityID
ORDER BY e.BusinessEntityID,jc.JobCandidateID